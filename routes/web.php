<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});

Route::get('/test', function(){
    return "OK";
});

Route::get('/tugas', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function(){
    return view('master');
});

Route::get('/', function(){
    return view('tugas_blade_template.tugas');
});
Route::get('/data-tables', function(){
    return view('tugas_blade_template.datatable');
});
